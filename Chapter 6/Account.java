import java.math.BigDecimal;
import java.math.RoundingMode;

public class Account {
    
    // instance variables    
    private double balance;
    private String name;

    // Default constructor
    public Account() {
    }
    
    // Constructor with arguments
    public Account(double balance, String name) {
        this.setBalance(balance);
        this.setName(name);
    }

    /**
     * Getter for balance
     * @return
     */
    public double getBalance() {
        return this.balance;
    }

    /**
     * Setter for balance
     * @param balance
     */
    public void setBalance(double balance) {
        this.balance = round(balance, 2);
    }

    /**
     * Getter for name
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Increases balance by 10%
     */
    public void addInterest() {
        this.setBalance(this.getBalance() * 1.1);
    }

    /**
     * Round doubles with BigDecimal
     * @param value
     * @param places
     * @return
     */
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
     
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public String toString() {
        return "{" +
            " balance='" + getBalance() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }

    /**
     * Print out account details
     */
    public void printDetails() {
        System.out.printf("Account name: %s, Balance: £%.2f\n\n", this.getName(), this.getBalance());
    }

}