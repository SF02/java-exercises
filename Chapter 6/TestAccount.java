import java.util.ArrayList;

public class TestAccount {
    
    public static void main(String[] args) {
        Account myAccount = new Account(1000.50, "Platypus");

        //myAccount.printDetails();
        //myAccount.addInterest();
        //myAccount.printDetails();

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
       
       for (int i = 0; i < arrayOfAccounts.length ; i++) {
        Account newAccount = new Account(amounts[i], names[i]);
        arrayOfAccounts[i] = newAccount;
        /*
        arrayOfAccounts[i].printDetails();
        arrayOfAccounts[i].addInterest();
        arrayOfAccounts[i].printDetails();
        */
       }
       
       ArrayList<Account> listOfAccounts = new ArrayList<Account>();

       for (int i = 0; i < 5 ; i++) {
        Account newAccount = new Account(amounts[i], names[i]);
        listOfAccounts.add(i, newAccount);
        
        //arrayOfAccounts[i].printDetails();
        //arrayOfAccounts[i].addInterest();
        //arrayOfAccounts[i].printDetails();
        
       }

       listOfAccounts.forEach((account) -> System.out.printf("Account name: %s, Balance: £%.2f\n\n", account.getName(), account.getBalance()));

    }
}
